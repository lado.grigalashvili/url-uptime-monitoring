'use strict';
/*
 *
 * Users Controller.
 *
 */

// Dependencies.
const ErrorResponse = require('../utils/errorResponse').default;
const asyncHandler = require('../middleware/async');
const User = require('../models/User');

// @desc        : Get all users.
// @Req. Data   : none.
// @route       : GET /api/v1/auth/users.
// @access      : Private/Admin.
exports.getUsers = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults);
});

// @desc        : Get single user.
// @Req. Data   : User ID.
// @route       : GET /api/v1/auth/users/:id
// @access      : Private/Admin.
exports.getUser = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.params.id);

    return res.status(200).json({
        success: true,
        data: user,
    });
});

// @desc        : Create user.
// @Req. Data   : firstName, lastName, phone, email.
// @route       : POST /api/v1/auth/users/.
// @access      : Private/Admin.
exports.createUser = asyncHandler(async (req, res, next) => {
    const user = await User.create(req.body);

    return res.status(201).json({
        success: true,
        data: user,
    });
});

// @desc        : Update user.
// @Req. Data   : User ID && firstName || lastName || phone || email.
// @route       : PUT /api/v1/auth/users/:id.
// @access      : Private/Admin.
exports.updateUser = asyncHandler(async (req, res, next) => {
    const user = await User.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
    });

    return res.status(200).json({
        success: true,
        data: user,
    });
});

// @desc        : Delete user.
// @Req. Data   : User ID.
// @route       : DELETE /api/v1/auth/users/:id.
// @access      : Admin.
exports.deleteUser = asyncHandler(async (req, res, next) => {
    await User.findByIdAndDelete(req.params.id);

    return res.status(200).json({
        success: true,
        data: {},
    });
});