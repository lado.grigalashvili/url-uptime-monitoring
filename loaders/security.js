'use strict';
/*
 *
 * Primary file for the API Security Middlewares.
 *
 */

// Dependencies.
const helmet = require('helmet');
const xss = require('xss-clean');
const hpp = require('hpp');
const rateLimit = require('express-rate-limit');
const mongoSanitize = require('express-mongo-sanitize');

// Rate limiting
const limiter = rateLimit({
    windowsMs: 10 * 60 * 1000, // 10 mins
    max: 1000,
});

module.exports = function (app) {
    // Set security headers.
    app.use(helmet());
    // Prevent XSS attacks.
    app.use(xss());
    // Prevent http param pollution.
    app.use(hpp());
    // Use to limit repeated requests.
    app.use(limiter);
    // Prevent MongoDB Operator Injection.
    app.use(mongoSanitize());
};