'use strict';
/*
 *
 * Primary file for the API Utility Middlewares.
 *
 */

// Dependencies.
const path = require('path');
const express = require('express');
const cookieParser = require('cookie-parser');
const fileupload = require('express-fileupload');
const cors = require('cors');
const errorHandler = require('../middleware/error');

module.exports = function (app) {
    // Body parser.
    app.use(express.json());
    // Error Handler.
    app.use(errorHandler);
    // Cookie parser.
    app.use(cookieParser());
    // File uploading.
    app.use(fileupload());
    // Enable CORS.
    app.use(cors());
    // Set static folder.
    app.use(express.static(path.join(__dirname, 'public')));
};