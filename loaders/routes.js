'use strict';
/*
 *
 * Primary file for the API Routes.
 *
 */

// Dependencies.
const auth = require('../routes/auth');
const users = require('../routes/users');

module.exports = function (app) {
    // Mount routers.
    app.use('/api/v1/auth', auth);
    app.use('/api/v1/users', users);
};