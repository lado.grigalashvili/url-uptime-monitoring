'use strict';
/*
 *
 * Limit repeated requests to API.
 *
 */

// Dependencies.
const rateLimit = require('express-rate-limit');


// Rate limiting.
const limiter = rateLimit({
    windowsMs: 10 * 60 * 1000, // 10 mins
    max: 1000,
});

